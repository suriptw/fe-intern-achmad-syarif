import { FaLightbulb, FaBalanceScale } from "react-icons/fa";
import { BsBank } from "react-icons/bs";

const About = () => {
  const data = [
    {
      id: 1,
      icon: <FaLightbulb className="w-10 h-10 text-white" />,
      title: "INNOVATIVE",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specim",
      color: "bg-red-400",
      border: "border-red-500",
    },
    {
      id: 2,
      icon: <BsBank className="w-10 h-10 text-white" />,
      title: "LOYALTI",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specim",
      color: "bg-emerald-500",
      border: "border-emerald-600",
    },
    {
      id: 3,
      icon: <FaBalanceScale className="w-10 h-10 text-white" />,
      title: "RESPECT",
      desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specim",
      color: "bg-blue-400",
      border: "border-blue-500",
    },
  ];
  return (
    <div className="container mx-auto">
      <h1 className="text-4xl font-bold text-gray-800 text-center my-16">
        OUR VALUES
      </h1>

      <div className="grid grid-cols-1 lg:grid-cols-3 gap-y-10 w-5/6 mx-auto justify-items-center ">
        {data.map((item, index) => {
          const isLastItem = index === data.length - 1;
          return (
            <div
              key={item.id}
              className={`flex flex-col items-center max-w-[400px] border-2 relative ${item.border}  mx-5 p-5 ${item.color}`}
            >
              <div className="flex items-center justify-center w-20 h-20">
                {item.icon}
              </div>
              <h1 className="text-xl font-bold text-white">{item.title}</h1>
              <p className="text-white py-5 text-center">{item.desc}</p>
              {!isLastItem && (
                <div
                  className={`absolute w-3 h-3 ${
                    index % 3 === 2
                      ? ``
                      : `border-t-[50px] border-l-[55px] border-b-[50px] border-solid border-t-transparent border-b-transparent ${item.border} z-[-1] bottom-[-50px] rotate-90  lg:rotate-0 lg:top-1/3 lg:right-[-30px] `
                  }`}
                ></div>
              )}
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default About;
