import { useState } from "react";
import { FaBars } from "react-icons/fa";
import { GrClose } from "react-icons/gr";

const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <nav className="container mx-auto w-5/6 px-6 md:flex md:justify-between md:items-center z-50">
      {/* Desktop */}
      <div className="hidden md:flex items-center justify-between flex-grow">
        <div className="font-bold text-slate-700 text-xl">
          <a href="/">Company</a>
        </div>
        <div className="">
          <ul className="md:flex items-center space-x-4">
            <li className="text-slate-500 font-semibold hover:bg-slate-200 px-4 py-3 cursor-pointer relative group">
              <a href="#">ABOUT</a>
              <ul className="absolute top-full left-0 text-slate-500 font-semibold w-[180px] z-50 shadow-lg hidden bg-white group-hover:block">
                <li className="text-slate-500 font-semibold hover:bg-slate-700 hover:text-white px-4 py-3  cursor-pointer">
                  <a href="#">HISTORY</a>
                </li>
                <li className="text-slate-500 font-semibold hover:bg-slate-700 hover:text-white px-4 py-3  cursor-pointer">
                  <a href="#">VISION & MISSION</a>
                </li>
              </ul>
            </li>
            <li className="text-slate-500 font-semibold hover:bg-slate-200 px-4 py-3 cursor-pointer">
              <a href="#">OUR WORK</a>
            </li>
            <li className="text-slate-500 font-semibold hover:bg-slate-200 px-4 py-3 cursor-pointer">
              <a href="#">OUR TEAM</a>
            </li>
            <li className="text-slate-500 font-semibold hover:bg-slate-200 px-4 py-3 cursor-pointer">
              <a href="#">CONTACT </a>
            </li>
          </ul>
        </div>
      </div>

      {/* Mobile */}
      <div className="md:hidden z-50 py-5">
        <div className="flex items-center justify-between">
          <div className="font-bold text-slate-700 text-xl">Company</div>
          <button
            className="text-2xl cursor-pointer focus:outline-none"
            onClick={toggleMenu}
          >
            {isOpen ? <GrClose /> : <FaBars />}
          </button>
        </div>
        <div
          className={`mt-4 z-50 absolute bg-white w-full border  md:hidden ${
            isOpen ? "block" : "hidden"
          }`}
        >
          <ul className="flex flex-col space-y-4">
            <li className="text-slate-500 font-semibold hover:bg-slate-200 px-4 py-3 cursor-pointer relative group">
              <a href="#">ABOUT</a>
              <ul
                className={` top-full  text-slate-500 font-semibold w-full shadow-lg hidden bg-white group-hover:block`}
              >
                <li className="text-slate-500 font-semibold hover:bg-slate-700 hover:text-white px-4 py-3 cursor-pointer">
                  <a href="#">HISTORY</a>
                </li>
                <li className="text-slate-500 font-semibold hover:bg-slate-700 hover:text-white px-4 py-3 cursor-pointer">
                  <a href="#">VISION & MISSION</a>
                </li>
              </ul>
            </li>
            <li className="text-slate-500 font-semibold hover:bg-slate-200 px-4 py-3 cursor-pointer">
              <a href="#">OUR WORK</a>
            </li>
            <li className="text-slate-500 font-semibold hover:bg-slate-200 px-4 py-3 cursor-pointer">
              <a href="#">OUR TEAM </a>
            </li>
            <li className="text-slate-500 font-semibold hover:bg-slate-200 px-4 py-3 cursor-pointer">
              <a href="#">CONTACT</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
