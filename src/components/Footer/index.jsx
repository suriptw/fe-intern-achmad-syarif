import { BiLogoFacebookSquare, BiLogoTwitter } from "react-icons/bi";

const Footer = () => {
  return (
    <div>
      <footer className="flex flex-col items-center justify-center bg-gray-700 py-4 bottom-0">
        <p className="text-white">
          Copyright &copy; {new Date().getFullYear()}, PT Company
        </p>
        <div className="my-4 flex gap-2 items-center">
          <a
            href="https://www.facebook.com/yourfacebookpage"
            target="_blank"
            rel="noopener noreferrer"
            className="inline-block max-w-[25px]"
          >
            <BiLogoFacebookSquare size={30} color="white"/>
          </a>
          <a
            href="https://www.twitter.com/yourtwitterpage"
            target="_blank"
            rel="noopener noreferrer"
            className="inline-block max-w-[25px]"
          >
            <BiLogoTwitter size={30} color="white"/>
          </a>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
