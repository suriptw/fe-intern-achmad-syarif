import React, { useState, useEffect } from "react";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";

import img_2 from "../../assets/bg.jpg";
import img_1 from "../../assets/about-bg.jpg";

const sliders = [
  {
    image: img_1,
    text: "WE DONT HAVE THE BEST BUT WE HAVE THE GREATEST TEAM",
  },
  {
    image: img_2,
    text: "THIS IS PLACE WHERE TECHNOLOGY & CREATIVITY FUSED INTO DIGITAL CHEMISTRY",
  },
];

const Slider = () => {
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentIndex((prevIndex) => (prevIndex + 1) % sliders.length);
    }, 3000);

    return () => clearInterval(interval);
  }, []);

  const prevSlide = () => {
    setCurrentIndex(
      (prevIndex) => (prevIndex - 1 + sliders.length) % sliders.length
    );
  };

  const nextSlide = () => {
    setCurrentIndex((prevIndex) => (prevIndex + 1) % sliders.length);
  };

  const slideStyles = (index) => {
    const offset = (currentIndex - index) * 100;
    return {
      transform: `translateX(${offset}%)`,
      transition: "transform 0.7s ease-in-out",
    };
  };

  return (
    <div className="relative z-10">
      <div className="relative h-screen max-h-[50%] overflow-hidden">
        {sliders.map((image, index) => (
          <div
            key={index}
            className={`z-10 absolute top-0 left-0 w-full h-full`}
            style={slideStyles(index)}
          >
            <img
              src={image.image}
              alt={`Slide ${index + 1}`}
              className="object-cover w-full h-full z-10"
            />
            <div className="absolute bottom-1/4 right-2/4 lg:text-2xl lg:max-w-xl">
              <span className="inline-block font-bold px-5 py-3 bg-black bg-opacity-50 text-white">
                {image.text}
              </span>
            </div>
          </div>
        ))}
        <div className="absolute bottom-0 left-0 right-0 flex justify-center pb-3">
          {sliders.map((_, i) => (
            <div
              key={i}
              className={`w-4 h-4 mx-1 rounded-full z-10 cursor-pointer ${
                i === currentIndex ? "bg-slate-700" : "bg-gray-300"
              }`}
              onClick={() => setCurrentIndex(i)}
            ></div>
          ))}
        </div>
        <BsChevronLeft
          className="outline rounded-full p-2 absolute top-1/2 left-0 z-10 cursor-pointer mx-4 text-4xl hover:text-black  hover:outline-none text-white hover:bg-white  "
          size={35}
          onClick={prevSlide}
        />
        <BsChevronRight
          className="outline rounded-full p-2 absolute top-1/2 right-0 z-10 cursor-pointer mx-4 text-4xl hover:text-black hover:outline-none text-white hover:bg-white  "
          size={35}
          onClick={nextSlide}
        />
      </div>
    </div>
  );
};

export default Slider;
