import About from "./components/About";
import Contact from "./components/Contact";
import Navbar from "./components/Navbar";
import Slider from "./components/Slider";
import Footer from "./components/Footer";

const App = () => {
  return (
    <div>
      <Navbar />
      <Slider />
      <About />
      <Contact />
      <Footer />
    </div>
  );
};

export default App;
